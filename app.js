const express = require('express')
const cors = require('cors');
const jwt = require('jsonwebtoken')
const session = require('express-session');
const path = require('path');
// init the app
const app = express()

// static content
app.use("/static", express.static(__dirname + '/static'));
app.use(express.json({ limit: '50mb' }));


// cors, bodyparser
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).send("App works")
})

//session 
app.use(session({
    secret: 'secretKey',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));


// static
app.use(express.static(path.join(__dirname, 'static')));

//jwt 
function verifyToken(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send('Unauthorized request')
    }
    let token = req.headers.authorization.split(' ')[1]
    if (token === 'null') {
        return res.status(401).send('Unauthorized request')
    }
    let payload = jwt.verify(token, 'secretKey')
    if (!payload) {
        return res.status(401).send('Unauthorized request')
    }
    req.userId = payload.subject
    next()
}

// Routes

// login
const login = require('./routes/login');
app.use('/api/login', login);

// course
const course = require('./routes/course');
app.use('/api/course', course);

// event
const event = require('./routes/event');
app.use('/api/event', event);

// testimony
const testimony = require('./routes/testimony');
app.use('/api/testimony', testimony);

// use 3000 or port set in .env
const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})