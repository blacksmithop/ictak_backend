const express = require('express');
let app = express.Router();
const jwt = require('jsonwebtoken');

const UserData = require('../modal/user');


app.post('/', function (req, res) {
    console.log(req.body);
    let username = req.body.username;
    let password = req.body.password;
    UserData.findOne({ username: username, password: password }, function (err, user) {
        if (err) {
            console.log(1)

            console.log("Login failed")
            res.status(403).send({ error: 'User not found!' });
        }
        else if (user) {
            console.log(2)

            UserData.findOne({ username: username })
                .then(function (userdata) {
                    var user = userdata;
                    console.log("Login success")
                    let payload = { subject: username + password }
                    let token = jwt.sign(payload, 'secretKey')
                    res.status(200).send(
                        { token: token, data: user }
                    );
                });

        } else {
            console.log(3)
            res.status(403).send({ error: 'User not found!' });
        }
    });
});


app.post('/changepass', function (req, res) {

    let oldPwd = req.body.oldPwdpassword;
    let newPwd = req.body.newPwd;
    let emailNew = req.body.email;
    console.log("pass", req.body)

    // mongo check for user
    UserData.findOne({ email: emailNew, password: oldPwd }, function (err, user) {
        if (err) {
            console.log("Invalid credentials")
            res.status(403).send({ error: "Invalid email/password" });
        }
        else if (user) {
            console.log("Credentials accepted")
            UserData.findOne({ email: req.body.email })
                .then(function (userdata) {
                    var user = userdata;
                });

            UserData.findByIdAndUpdate({ _id: user._id },
                {
                    $set: {
                        "password": newPwd,

                    }
                })
                .then((respond) => {
                    if (respond) {
                        console.log('Password changed')
                        res.status(200).send({
                            data: "Password change success"
                        })
                    }
                    else {
                        console.log('Cannot change password', error)
                        res.status(400).send({
                            error: "Unable to change password, try again"
                        })
                    }
                });
        }
    });
})


module.exports = app;