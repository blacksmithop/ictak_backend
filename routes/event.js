const express = require('express');
let app = express.Router();
var nodemailer = require('nodemailer');

//email credentils
var mail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'oxiblurr2@gmail.com',
        pass: 'K#^D3#3r'
    }
});


const EventData = require('../modal/event');

app.get('/list', function (req, res) {
    EventData.find().sort({ index: 1 })
        .then(function (events) {
            res.status(200).send(events);
        });
});

app.get('/get/:id', (req, res) => {
    const id = req.params.id;
    EventData.findOne({ "_id": id })
        .then((event) => {
            res.send(event);
        });
})


app.post('/register', function (req, res) {
    var request = {
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        course_id: req.body.course_id,
    };
    console.log(request)


    var mailOptions = {
        from: 'angstycoder101@gmail.com',
        to: req.body.email,
        subject: 'Greetings from ICTAK',
        html: '<p>Congratulations on registering for an event with us</p><p>Please find the brochure attached</p>',
        attachments: [{
            filename: 'brochure.pdf',
            path: './public/brochures/sample.pdf'
        }]
    }
    mail.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

    res.send(200);
})

app.post('/add', function (req, res) {

    EventData.findOne().sort('-index').exec(function (err, course) {

        var event = {
            name: req.body.name,
            image_url: req.body.image_url,
            description: req.body.description,
            fee: req.body.fee,
            organizer: req.body.oragnizer,
            duration: req.body.duration,
            contact: req.body.contact,
        }

        var eventItem = new EventData(event);
        eventItem.save().then(function (data) {
            res.status(200).send({
                data: "Added new event"
            })
        }).catch(function (error) {
            res.status(400).send({
                error: "Could not add event"
            })

        });
    });

});

app.post('/remove', (req, res) => {
    console.log(req.body);
    id = req.body._id
    Coursedata.findByIdAndDelete({ '_id': id },
        (err, result) => {
            if (err) {
                res.status(200).send({
                    data: "Removed event"
                })
            } else {
                res.status(400).send({
                    data: "Could not remove event"
                })
            }
        });
});


app.post('/update', (req, res) => {

    id = req.body._id;
    var event = {
        name: req.body.name,
        image_url: req.body.image_url,
        description: req.body.description,
        fee: req.body.fee,
        organizer: req.body.oragnizer,
        duration: req.body.duration,
        contact: req.body.contact,
    }

    let updateEvent = { $set: event };
    EventData.findByIdAndUpdate({ "_id": id }, updateEvent)
        .then((respond) => {
            if (respond) {
                res.status(200).send({
                    data: "Updated event"
                })
            }
            else {
                res.status(400).send({
                    data: "Could not update event"
                })
            }
        });
});




module.exports = app;