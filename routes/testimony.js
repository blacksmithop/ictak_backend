const express = require('express');
let app = express.Router();

const Testimonialdata = require('../modal/testimony');

app.get('/list', function (req, res) {
    Testimonialdata.find().sort({ index: 1 })
        .then(function (events) {
            res.status(200).send(events);
        });
});

app.get('/get/:id', (req, res) => {
    const id = req.params.id;
    Testimonialdata.findOne({ "_id": id })
        .then((event) => {
            res.send(event);
        });
})


app.post('/add', function (req, res) {

    Testimonialdata.findOne().sort('-index').exec(function (err, testimony) {

        var testimony = {
            testimony_name: req.body.testimony_name,
            image_url: req.body.image_url,
            description: req.body.description,
            name: req.body.name,
            role: req.body.role,
            organisation: req.body.organisation,
        }

        var eventItem = new Testimonialdata(testimony);
        eventItem.save().then(function (data) {
            res.status(200).send({
                data: "Added new testimony"
            })
        }).catch(function (error) {
            res.status(400).send({
                error: "Could not add testimony"
            })

        });
    });

});

app.post('/remove', (req, res) => {
    console.log(req.body);
    id = req.body._id
    Testimonialdata.findByIdAndDelete({ '_id': id },
        (err, result) => {
            if (err) {
                res.status(200).send({
                    data: "Removed testimony"
                })
            } else {
                res.status(400).send({
                    data: "Could not remove testimony"
                })
            }
        });
});


app.post('/update', (req, res) => {

    id = req.body._id;
    var testimony = {
        testimony_name: req.body.testimony_name,
        image_url: req.body.image_url,
        description: req.body.description,
        name: req.body.name,
        role: req.body.role,
        organisation: req.body.organisation,
    }

    let updateTestimony = { $set: testimony };
    Testimonialdata.findByIdAndUpdate({ "_id": id }, updateTestimony)
        .then((respond) => {
            if (respond) {
                res.status(200).send({
                    data: "Updated testimony"
                })
            }
            else {
                res.status(400).send({
                    data: "Could not update testimony"
                })
            }
        });
});




module.exports = app;