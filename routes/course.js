const express = require('express');
let app = express.Router();
var nodemailer = require('nodemailer');
const path = require('path');
var mongoXlsx = require('mongo-xlsx');

var options = {
    save: true,
    path: "./static",
}

//email credentils
var mail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'oxiblurr2@gmail.com',
        pass: 'K#^D3#3r'
    }
});

const Coursedata = require('../modal/course');
const Register = require('../modal/courseregister');


app.get('/list', function (req, res) {
    Coursedata.find().sort({ index: 1 })
        .then(function (courses) {
            res.send(courses);
        });
});

app.get('/registerlist', function (req, res) {
    Register.find().sort({ index: 1 })
        .then(function (courses) {
            res.send(courses);
        });
});

app.get('/excel', function (req, res) {
    Register.find().sort({ index: 1 })
        .then(function (courses) {
            var model = mongoXlsx.buildDynamicModel(courses);
            mongoXlsx.mongoData2Xlsx(courses, model, options, function (err, data) {
                console.log('File saved at:', data.fullPath);
                res.status(200).send(data.fullPath);

            });
        });
});

app.get('/get/:id', (req, res) => {
    const id = req.params.id;
    Coursedata.findOne({ "_id": id })
        .then((course) => {
            res.send(course);
        });
})


app.post('/register', function (req, res) {
    var request = {
        first: req.body.firstname,
        last: req.body.lastname,
        email: req.body.email,
        phone: req.body.phone,
        course_id: req.body.course_id,
    };
    Coursedata.findOne({ "_id": req.body.course_id })
        .then((course) => {
            request['course_id'] = course.title;
        });
    console.log(request);

    var courseItem = new Register(request);
    courseItem.save().then(function (data) {
        res.status(200).send({
            data: "Registered for course"
        })
    }).catch(function (error) {
        res.status(400).send({
            error: "Failed to register"
        })

    });

    var mailOptions = {
        from: 'angstycoder101@gmail.com',
        to: req.body.email,
        subject: 'Greetings from ICTAK',
        html: '<p>Congratulations on registering for an online course with us</p><p>Please find the brochure attached</p>',
        attachments: [{
            filename: 'brochure.pdf',
            path: './public/brochures/sample.pdf'
        }]
    }
    mail.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

    res.status(200);
})

app.post('/add', function (req, res) {

    var course = {
        title: req.body.title,
        image_url: req.body.image_url,
        description: req.body.description,
        category: req.body.category,
        criteria: req.body.criteria,
        fee: req.body.fee,
        duration: req.body.duration,
        internship_partner: req.body.internship_partner,
    }
    console.log(course['fee']);
    var courseItem = new Coursedata(course);
    courseItem.save().then(function (data) {
        res.status(200).send({
            data: "Added new course"
        })
    }).catch(function (error) {
        console.log(error);
        res.status(400).send({
            error: "Could not add course"
        })

    });

});


app.post('/remove', (req, res) => {
    console.log(req.body);
    id = req.body._id
    Coursedata.findByIdAndDelete({ '_id': id },
        (err, result) => {
            if (err) {
                res.status(200).send({
                    data: "Removed course"
                })
            } else {
                res.status(400).send({
                    data: "Could not remove course"
                })
            }
        });
});


app.post('/update', (req, res) => {

    id = req.body._id;
    var item = {
        title: req.body.title,
        image_url: req.body.image_url,
        description: req.body.description,
        category: req.body.category,
        criteria: req.body.criteria,
        fee: req.body.fee,
        canRefund: req.body.canRefund,
        duration: req.body.duration,
        internship_partner: req.body.internship_partner,
    }

    let updateCourse = { $set: item };
    Coursedata.findByIdAndUpdate({ "_id": id }, updateCourse)
        .then((respond) => {
            if (respond) {
                res.status(200).send({
                    data: "Updated course"
                })
            }
            else {
                res.status(400).send({
                    data: "Could not update course"
                })
            }
        });
});



module.exports = app;