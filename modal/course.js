const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://testuser:1mfNIeRpAyP4njru@cluster0.2yxb6.mongodb.net/ICTAK?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Schema = mongoose.Schema;

const CourseSchema = new Schema({
    title: String,
    image_url: String,
    description: String,
    category: String,
    criteria: String,
    fee: String,
    duration: Number,
    internship_partner: String,
});

var Coursedata = mongoose.model('course', CourseSchema);

module.exports = Coursedata;