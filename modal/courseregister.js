const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://testuser:1mfNIeRpAyP4njru@cluster0.2yxb6.mongodb.net/ICTAK?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Schema = mongoose.Schema;

const CourseRegisterSchema = new Schema({
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    course_id: String
});

var CourseRegdata = mongoose.model('courseregister', CourseRegisterSchema);

module.exports = CourseRegdata;