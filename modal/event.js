const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://testuser:1mfNIeRpAyP4njru@cluster0.2yxb6.mongodb.net/ICTAK?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Schema = mongoose.Schema;

const EventSchema = new Schema({
    name: String,
    image_url: String,
    description: String,
    duration: Number,
    organizer: String,
    fee: String,
    contact: Number,
});

var Eventdata = mongoose.model('event', EventSchema);

module.exports = Eventdata;