const mongoose = require('mongoose');

mongoose.connect('url', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Schema = mongoose.Schema;

const PartnershipSchema = new Schema({
    name: String,
    email: String,
    phone: String,
    organization: String,
    address: String,
    website: String,
    isApproved: Boolean
});


var PartnerApplication = mongoose.model('partner', PartnershipSchema);

module.exports = PartnerApplication;
