const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://testuser:1mfNIeRpAyP4njru@cluster0.2yxb6.mongodb.net/ICTAK?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Schema = mongoose.Schema;

const SignUpSchema = new Schema({
    isAdmin: Boolean,
    firstName: String,
    lastName: String,
    username: String,
    password: String,
    email: String
});

var SignUpData = mongoose.model('user', SignUpSchema);

module.exports = SignUpData;