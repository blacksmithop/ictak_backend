const mongoose = require('mongoose');
mongoose.connect('url', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Schema = mongoose.Schema;

const EnquirySchema = new Schema({
    name: String,
    email: String,
    subject: String,
    message: String,
    hasResponded: Boolean,
    date: Date
});


var EnquiryData = mongoose.model('enquiry', EnquirySchema);

module.exports = EnquiryData;