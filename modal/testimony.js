const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://testuser:1mfNIeRpAyP4njru@cluster0.2yxb6.mongodb.net/ICTAK?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});


const Schema = mongoose.Schema;

const TestimonialSchema = new Schema({
    course_name: String,
    name: String,
    role: String,
    organisation: String,
    description: String,
    image_url: String
});

var Testimonialdata = mongoose.model('testimonial', TestimonialSchema);

module.exports = Testimonialdata;